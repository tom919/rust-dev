fn main(){
    let mut ctr = 1;
    let max_ctr = 5;
    println!("while loop prog start...");
    while ctr < max_ctr {
        println!("value : {}", ctr);
        ctr = ctr+1;
    }
    println!("prog end ...")
}