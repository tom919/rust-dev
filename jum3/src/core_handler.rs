
use actix_web::{
    HttpResponse,
};



// simple aliasing makes the intentions clear and its more readable


pub fn hellojum() -> HttpResponse {
    let greeting = "hello tom's";
    HttpResponse::Ok().json(greeting)
}

